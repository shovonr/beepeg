(function ($) {
  $('.photo').each(function (i) {
    var $this = $(this);
    var $image = $this.find('img');
    var source = $image.attr('src');
    if (/\.bpg$/.test(source)) {
      var canvas = document.createElement('canvas');
      var context = canvas.getContext('2d');
      decode(source, function (err, imageData) {
        if (err) { throw new Error('Some error occurred'); }
        canvas.width = imageData.width;
        canvas.height = imageData.height;
        var imgData = context.createImageData(canvas.width, canvas.height);
        for (var i = 0; i < imgData.data.length; i++) {
          imgData.data[i] = imageData.data[i];
        }
        context.putImageData(imgData, 0, 0);
        $image.remove();
        $this.append(canvas);
      });
    }
  });

  // TODO: a lot of the dependent code does not throw any error if the image
  //   is invalid.
  function decode(source, callback) {
    var worker = new Worker('/lib/decoderWorker.js');
    worker.postMessage(source);
    worker.onmessage = function (e) {
      callback(null, e.data);
    };
  }
}(jQuery));