importScripts('/lib/bpgdec.js');

self.addEventListener("message", function(e){
  var img = new BPGDecoder();
  img.onload = function () {
    self.postMessage(this.imageData);
  };
  img.load(e.data);
});