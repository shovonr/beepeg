var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');
var runSequence = require('run-sequence');
var nodemon = require('gulp-nodemon');

/*
 * Handles an error event.
 */
function swallowError(error) {
  gutil.log(error);
  this.emit('end');
}

var paths = {
  less: path.join(__dirname, 'styles', '*.less')
}

/*
 * Compiles all LESS style sheets that are "local" to specific modules.
 */
gulp.task('less', function () {
  return gulp.src(paths.less)
    .pipe(less({
      paths: [ path.join(__dirname, 'src') ]
    }))
    .on('error', swallowError)
    .pipe(gulp.dest(path.join(__dirname, 'public', 'styles')));
});

gulp.task('build', function () {
  runSequence('less');
});

gulp.task('watch', function () {
  gulp.watch(paths.less, ['less']);
});

gulp.task('server', function () {
  nodemon({
    script: 'app.js',
    ignore: ['public/**', 'node_modules/**', 'gulpfile.js']
  });
});

gulp.task('default', function () {
  runSequence('build', [ 'watch', 'server' ])
});