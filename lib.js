var child_process = require('child_process');
var async = require('async');
var crypto = require('crypto');
var path = require('path');
var db = require('./db');
var fs = require('fs');
var mkdirp = require('mkdirp');

var RANDOM_BYTES_COUNT = 12;

var crypto = require('crypto');

/** Sync */
function randomString(length, chars) {
  if(!chars) {
    throw new Error('Argument \'chars\' is undefined');
  }

  var charsLength = chars.length;
  if(charsLength > 256) {
    throw new Error('Argument \'chars\' should not have more than 256 characters'
        + ', otherwise unpredictability will be broken');
  }

  var randomBytes = crypto.randomBytes(length)
  var result = new Array(length);

  var cursor = 0;
  for (var i = 0; i < length; i++) {
    cursor += randomBytes[i];
    result[i] = chars[cursor % charsLength]
  };

  return result.join('');
}

/** Sync */
function randomAsciiString(length) {
  return randomString(length,
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
}

/*
 * Takes the image file at location specifed by `source`, converts it to a BPG
 * image, and stores at location specified by `destination`.
 */
module.exports.convertToBPG = convertToBPG;
function convertToBPG(source, destination, callback) {
  var bpgenc = child_process.spawn('bpgenc', [ source, '-o', destination ]);
  bpgenc.on('close', function (code) {
    if (code) {
      return callback(
        new Error('An error occurred while trying convert image to BPG')
      );
    }
    return callback(null);
  });
};

function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}

/*
 * Migrates the specified file to some other place (for now, just in a `uploads`
 * folder).
 */
module.exports.migrateFile = migrateFile;
function migrateFile(source, fileID, callback) {
  var extname = path.extname(source);
  async.waterfall([
    // Just for good measures, create the folder where we will be storing the
    // uploaded file.
    function (callback) {
      mkdirp(path.join(__dirname, 'public', 'uploads'), function (err) {
        if (err) { return callback(err); }
        callback(null);
      });
    },
    function (callback) {
      var filepath = path.join(
        __dirname, 'public', 'uploads', fileID + extname
      );
      copyFile(source, filepath, function (err) {
        if (err) { return callback(err); }
        callback(null);
      });
    }
  ], function (err) {
    if (err) { return callback(err); }
    callback(null);
  })
}

// TODO: test this.
module.exports.handleUploadedFile = function (filepath, callback) {
  async.waterfall([
    function (callback) {
      // Our BPG conversion utility only accepts JPEG and PNG files. On top of
      // that, it only determines the files format based on the file name's
      // extension.
      //
      // Because the uploaded file (which it is uploaded to a temporary
      // directory) doesn't get a file extension, give it the `.jpg`.
      // TODO: add support for PNG.
      fs.rename(filepath, filepath + '.jpg', function (err) {
        if (err) { return callback(err); }
        callback(null);
      });
    },
    function (callback) {
      // Convert our image to BPG using the conversion utilty.
      convertToBPG(filepath + '.jpg', filepath + '.bpg', function (err) {
        if (err) { return callback(err); }
        callback(null);
      });
    },
    function (callback) {
      // Do note that we are not adding the file extension name.
      var fileID = crypto.randomBytes(RANDOM_BYTES_COUNT).toString('base64');

      // ATTENTION: need to change *where* the files are eventually stored? Take
      //   a look at the `migrateFile` method. That is where we decide
      //   *where to* put the converted file.
      async.waterfall([
        // Migrate our files.
        function (callback) {
          var paths = [filepath + '.bpg', filepath + '.jpg'];
          async.eachSeries(paths, function (fpath, callback) {
            migrateFile(fpath, fileID, function (err) {
              if (err) { return callback(err); }
              callback(null);
            })
          }, function (err) {
            if (err) { return callback(err); }
            callback(null);
          });
        },

        // Get the file sizes, and store the new file into the database.
        function (callback) {
          var jpgSize, bpgSize;
          async.waterfall([
            function (callback) {
              fs.stat(filepath + '.jpg', function (err, stat) {
                if (err) { return callback(err); }
                fs.unlink(filepath + '.jpg', function (err) {
                  if (err) { console.error(err); }
                });
                jpgSize = stat.size;
                callback(null);
              });
            },
            function (callback) {
              fs.stat(filepath + '.bpg', function (err, stat) {
                if (err) { return callback(err); }
                fs.unlink(filepath + '.bpg', function (err) {
                  if (err) { console.error(err); }
                })
                bpgSize = stat.size;
                callback(null);
              });
            },
            function (callback) {
              db.insertImage({
                id: fileID,
                oldSize: jpgSize,
                newSize: bpgSize,
                timeCreated: new Date()
              }, function (err) {
                if (err) { return callback(err); }
                callback(null);
              });
            }
          ], function (err) {
            if (err) { return callback(err); }
            callback(null);
          })
        }
      ], function (err) {
        if (err) { return callback(err); }
        callback(null, fileID);
      })
    }
  ], function (err) {
    if (err) { return callback(err); }
    return callback(null);
  });
};

// TODO: test this.
module.exports.imageToURL = function (image) {
  return '/' + path.join('uploads', image.id + '.bpg')
};