# BeePeG

> An app that allows you to upload images, and convert them to BPG format

## Developing

First, you'll need to have [`libbpg`](http://bellard.org/bpg/) installed. On OS X, the best way to install it would be through [Homebrew](http://brew.sh/). On Linux, the default compilation settings should suffice for compiling and then installing the binaries, as well as setting up the correct symlinks. On Windows, you can download the compiled binaries, and putting the executable in your `PATH`.

To run the app in development mode, you will first have to ensure that you have MongoDB listening on address 127.0.0.1, and on port 27017. To do that, you would run MongoDB like so:

```shell
$ mongod [options] --port=27017
```

Next, be sure you have installed all dependencies for the app, locally, by first `cd`ing into the app's root folder, and then running

```shell
$ npm install
```

Finally:

```shell
$ npm run develop
```