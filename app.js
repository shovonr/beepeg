var express = require('express');
var jade = require('jade');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var mkdirp = require('mkdirp');
var async = require('async');
var lib = require('./lib');
var db = require('./db');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

// TODO: test this.
app.get('/', function (req, res) {
  db.getImages(function (err, docs) {
    if (err) {
      console.error(err);
      return res.status(500).render('error', {});
    }
    res.render('index', {
      imagenames: docs.map(function (doc) {
        return lib.imageToURL(doc);
      })
    })
  });
});

// TODO: test this.
app.get('/upload', function (req, res) {
  res.render('upload', {});
});

// TODO: standardize on what response to send to the user.
// TODO: test this.
app.post('/upload', function (req, res, next) {
  var image;

  var form = new formidable.IncomingForm();

  form.on('error', function (err) {
    next(err);
  });

  async.waterfall([
    // Get the file's name based on the parsed form data.
    function (callback) {
      form.parse(req, function (err, fields, files) {
        if (err) { return callback(err); }
        if (!files.image) {
          return res.status(400).send('No image file supplied.');
        }
        callback(null, files);
      });
    },

    // Handle the uploaded file.
    function (files, callback) {
      lib.handleUploadedFile(files.image.path, function (err, remote) {
        if (err) { return callback(err); }
        callback(null);
      });
    }
  ], function (err) {
    if (err) { return next(err); }
    res.status(200).send('form contents uploaded!');
  });
});

app.listen(3000, function () {
  console.log(this.address().port);
});