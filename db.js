var MongoClient = require('mongodb').MongoClient;
var EventEmitter = require('events').EventEmitter;
var inspector = require('schema-inspector');

var emitter = new EventEmitter();

var connected = false;
var connectionError = null;
var db = null;

function getDB(callback) {
  if (!connected && connectionError === null) {
    emitter.addListener('connected', function (db) {
      callback(null, db);
    });
    emitter.addListener('error', function (err) {
      callback(err);
    });
    return;
  } else if (connectionError !== null) {
    return setImmediate(function () {
      callback(connectionError);
    });
  }
  return setImmediate(function () {
    callback(null, db);
  });
}

MongoClient.connect('mongodb://localhost:27017/beepeg', function (err, _db) {
  if (err) {
    connectionError = err;
    return emitter.emit('error', err);
  }

  connected = true;
  db = _db;
  return emitter.emit('connected', db);
});

// TODO: test this.
module.exports.insertImage = insertImage;
function insertImage(image, callback) {
  var errors = inspector.validate({
    type: 'object',
    properties: {
      id: { type: 'string' },
      oldSize: { type: 'integer' },
      newSize: { type: 'integer' },
      timeCreated: { type: 'date' }
    }
  }, image);
  if (!errors.valid) {
    return setImmediate(function () {
      var err = new Error('Invalid properties');
      err.props = errors;
      callback(err);
    });
  }

  getDB(function (err, db) {
    if (err) { return callback(err); }
    db.collection('images').insert([
      image
    ], function (err) {
      if (err) { return callback(err); }
      callback(null);
    });
  });
}

// TODO: test this.
module.exports.getImages = getImages;
function getImages(callback) {
  getDB(function (err, db) {
    if (err) { return callback(err); }
    db.collection('images').find({}).toArray(function (err, docs) {
      if (err) { return callback(err); }
      callback(null, docs);
    });
  });
}